#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

function _publish() {
  local -r base_dir="$(dirname "$(realpath "${0%/*}")")"

  local -r package_name="$(cat "${base_dir}/.name")"
  local -r package_version="$(cat "${base_dir}/.version")"
  local -r package_build="$(cat "${base_dir}/.build")"
  local -r package_arch="$(cat "${base_dir}/.arch")"

  local -r remote="rpms.jlisher.com"
  local -r remote_base_dir="/srv/rpms"

  local -a sign_paths=()
  local -r result_base="${base_dir}/builds"
  local -ar build_roots=("centos-stream-8-x86_64" "centos-stream-9-x86_64")

  for cfg in "${build_roots[@]}"; do
    result_dir="${cfg//-/\/}"
    release_dir="${result_dir%/*}"
    release_dir="${release_dir/centos\/stream/el}"
    release_arch="${result_dir##*/}"
    release_version="${release_dir/fedora/fc}"
    release_version="${release_version/\//}"
    release_build="${package_build%.*}.${release_version}"
    release_rpm_dir="${release_dir}/${release_arch}"
    release_srpm_dir="${release_dir}/src"
    release_rpm="${package_name}-${package_version}-${release_build}.${package_arch}.rpm"
    release_rpm_latest="${package_name}-latest.${release_version}.${package_arch}.rpm"
    release_srpm="${package_name}-${package_version}-${release_build}.src.rpm"
    release_rpm_keys="jlisher-gpg-keys-${package_version}-${release_build}.${package_arch}.rpm"
    release_rpm_keys_latest="jlisher-gpg-keys-latest.${release_version}.${package_arch}.rpm"

    rm -rf "${result_base}/${result_dir}"/*.log
    ssh "${remote}" mkdir -p "${remote_base_dir}/${release_rpm_dir}" "${remote_base_dir}/${release_srpm_dir}"
    scp "${result_base}/${result_dir}/${release_rpm}" "${remote}:${remote_base_dir}/${release_rpm_dir}/"
    scp "${result_base}/${result_dir}/${release_rpm}" "${remote}:${remote_base_dir}/${release_dir}/${release_rpm_latest}"
    scp "${result_base}/${result_dir}/${release_srpm}" "${remote}:${remote_base_dir}/${release_srpm_dir}/"

    scp "${result_base}/${result_dir}/${release_rpm_keys}" "${remote}:${remote_base_dir}/${release_rpm_dir}/"
    scp "${result_base}/${result_dir}/${release_rpm_keys}" "${remote}:${remote_base_dir}/${release_dir}/${release_rpm_keys_latest}"

    sign_paths+=("${remote_base_dir}/${release_rpm_dir}/${release_rpm}")
    sign_paths+=("${remote_base_dir}/${release_dir}/${release_rpm_latest}")
    sign_paths+=("${remote_base_dir}/${release_srpm_dir}/${release_srpm}")

    sign_paths+=("${remote_base_dir}/${release_rpm_dir}/${release_rpm_keys}")
    sign_paths+=("${remote_base_dir}/${release_dir}/${release_rpm_keys_latest}")
  done

  scp "${base_dir}/install-jlisher-el-repo.sh" "${remote}:${remote_base_dir}/el/"
  sign_paths+=("${remote_base_dir}/el/install-jlisher-el-repo.sh")

  # shellcheck disable=SC2029
  ssh "${remote}" sign-and-sum "${sign_paths[@]}"
}

_publish
