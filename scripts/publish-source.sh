#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

function _publish() {
  set -euo pipefail

  local -r base_dir="$(dirname "$(realpath "${0%/*}")")"

  local -r package_name="$(cat "${base_dir}/.name")"
  local -r package_version="$(cat "${base_dir}/.version")"

  local -r build_tar="${package_name}-${package_version}.tar.gz"

  local -r remote="sources.jlisher.com"
  local -r remote_dir="/srv/sources/${package_name}/"

  ssh "${remote}" mkdir -p "${remote_dir}"
  scp "${base_dir}/${build_tar}" "${remote}:${remote_dir}"
  # shellcheck disable=SC2029
  ssh "${remote}" sign-and-sum "${remote_dir}/${build_tar}"
}

_publish
