#!/usr/bin/env bash
set -euo pipefail
IFS=$'\n\t'

function _build() {
  local -r base_dir="$(dirname "$(realpath "${0%/*}")")"

  local -r package_name="$(cat "${base_dir}/.name")"
  local -r package_version="$(cat "${base_dir}/.version")"

  local -r build_dir="${package_name}-${package_version}"
  local -r build_tar="${build_dir}.tar.gz"

  pushd "${base_dir}"

  ! [[ -d "${build_dir}" ]] || {
    rm -rf "${build_dir}"
  }
  mkdir -p "${build_dir}"

  cp -R -t "${build_dir}" "${base_dir}"/{rpm-gpg,yum.repos.d,install-jlisher-el-repo.sh,README.md,LICENSE}

  tar -acvf "${build_tar}" "${build_dir}"

  rm -rf "${build_dir}"

  popd
}

_build
