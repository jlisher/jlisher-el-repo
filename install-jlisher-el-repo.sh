#!/bin/bash
set -euxo pipefail

# create new temp directory and enter it.
# NOTE: we do not need to capture the directory name here, as it will be available in the `${PWD}` environment variable,
#       then the `${OLDPWD}` environment variable after we `popd` at the end.
pushd "$(mktemp -d)" 1>/dev/null

# Create a trap function to handle errors.
_trap_err() {
  echo "ERROR: a command returned with a non-zero code."
  echo "ERROR: Files are still available in the '${PWD}' directory."
  # Leave temp directory
  popd 1>/dev/null
}

trap _trap_err ERR

# Fetch the public keyring and sha256sum.
# It's a little pointless to verify the GPG signature using the file as the keyring.
# So not going to bother, however, feel free to do that manually.
gpg_keyring_filename="gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
curl --remote-name-all --location \
  "https://sources.jlisher.com/${gpg_keyring_filename}{,.sha256sum}"

# Make sure our keyring is not broken (can be caused by network issues).
sha256sum -c "${gpg_keyring_filename}.sha256sum"

# Confirm GPG Fingerprint. Make sure we actually have the correct keyring.
# NOTE: If the fingerprint doesn't match, will fail with the message: "gpg: error reading key: No public key".
gpg --no-verbose --homedir "${PWD}" --no-default-keyring --keyring "${gpg_keyring_filename}" \
  --list-keys "17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA"

# Define package to install
el_release="$(rpm -E %rhel)"
package_filename="jlisher-el-repo-latest.el${el_release}.noarch.rpm"
gpg_package_filename="jlisher-gpg-keys-latest.el${el_release}.noarch.rpm"

# Fetch the `jlisher-el-repo` and `jlisher-gpg-keys` rpm package, checksum and GPG signature files
curl --remote-name-all --location \
  "https://rpms.jlisher.com/el/${el_release}/${package_filename}{,.sha256sum}{,.asc}" \
  "https://rpms.jlisher.com/el/${el_release}/${gpg_package_filename}{,.sha256sum}{,.asc}"

# Verify the checksums` GPG signature
gpgv --keyring "${PWD}/${gpg_keyring_filename}" \
  "${package_filename}.sha256sum.asc" "${package_filename}.sha256sum"

gpgv --keyring "${PWD}/${gpg_keyring_filename}" \
  "${gpg_package_filename}.sha256sum.asc" "${gpg_package_filename}.sha256sum"

# Verify packages consistency
sha256sum -c "${package_filename}.sha256sum" "${gpg_package_filename}.sha256sum"

# Verify packages GPG signature
gpgv --keyring "${PWD}/${gpg_keyring_filename}" \
  "${package_filename}.asc" "${package_filename}"

gpgv --keyring "${PWD}/${gpg_keyring_filename}" \
  "${gpg_package_filename}.asc" "${gpg_package_filename}"

# Install packages script
_install_packages() {
  # Determine if command exits
  _exits() { type -t "${1}" 1>/dev/null 2>&1; }

  # If `sudo` is installed, prefix the install commands with `sudo`
  _exits sudo && SUDO="sudo" || SUDO=""

  # Install packages with dnf
  ! _exits dnf || { ${SUDO} dnf install "${@}"; return $?; }

  # Install packages with rpm-ostree
  ! _exits rpm-ostree || { ${SUDO} rpm-ostree install "${@}"; return $?; }

  # Install packages with rpm
  ! _exits rpm || { ${SUDO} rpm -iv "${@}"; return $?; }

  # Return with non-zero code to error the script
  echo "ERROR: unsupported package manager." 1>&2
  return 1
}

_install_packages "${package_filename}" "${gpg_package_filename}"

# Import GPG keys
rpmkeys --import "/etc/pki/rpm-gpg/RPM-GPG-KEY-jlisher-el-${el_release}"

# return to original directory
popd 1>/dev/null

# cleanup files if not requested to keep them
[[ ${NO_CLEANUP:-0} -ne 0 ]] || {
  rm -r "${OLDPWD}"
}

echo "SUCCESS: jlisher-el-repo installed."
echo "SUCCESS: To enable the repo you can run:"
echo "SUCCESS:    dnf config-manager --set-enabled jlisher-el"
echo "SUCCESS: or, for systems without dnf config-manager:"
echo "SUCCESS:    sed -E -e '3,6s/^(enabled)\=.*$/\1\=1/' /etc/yum.repos.d/jlisher-el.repo"
