# jlisher-el-repo

> WARNING: At the moment, this repo may be down from time to time, for upto 3 hours at a time.
> This is out of my control, partly. However, if there is interest, I am will to migrate to a more stable hosting solution.

RPM repositories

I have tried to keep to the [recommendations provided by Fedora][fedora-packaging].

## Dependencies

These should be installed by default, but here is a list just in case.

- [`coreutils`][coreutils] or [`coreutils-single`][coreutils]: to provide the basic utilities and environment
  - This also provides `sha256sum`, which we need to verify checksums
- [`curl`][curl]: for fetching remote files ([`wget`][wget] can work too, but I prefer [`curl`][curl])
- [`gnupg2`][gnupg2]: for package GPG signature verification
- [`dnf`][dnf]: to actually use the repo

## Installation

The installation process is fairly standard. I have also added the [recommended verification options](#Verification), suggested by Fedora.

### Verification

All files have the following additional files available for those wanting extra verification:

- `{file}.asc`: detached GPG signature
  - Used to verify the authenticity of the downloaded files
- `{file}.sha256sum`: checksum
  - Used to verify the consistency of the downloaded files
- `{file}.sha256sum.asc`: checksum detached GPG signature
  - Used to verify the authenticity of the downloaded checksums

<details>
<summary>More Information (click to expand)</summary>

You can use `{,.sha256sum}{,.asc}` with brace expansion as a short hand to get all files.

For example:

```sh
filename="gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
remote_url="https://sources.jlisher.com/${filename}"
curl --remote-name-all --location "${remote_url}{,.sha256sum}{,.asc}"
```

will download these files in the current working directory:

| Remote URL                    | Local Path                            |
| ---                           | ---                                   |
| `${remote_url}`               | `${PWD}/${filename}`                  |
| `${remote_url}.asc`           | `${PWD}/${filename}.asc`              |
| `${remote_url}.sha256sum`     | `${PWD}/${filename}.sha256sum`        |
| `${remote_url}.sha256sum.asc` | `${PWD}/${filename}.sha256sum.asc`    |

</details>

All RPM packages are also signed using the same GPG key.

#### Checksum

Simply download the `${file}.sha256sum` next to `${file}` (substituting `${file}` with the filename), and run:

```sh
filename="gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
sha256sum -c "${filename}.sha256sum"
```

If this passes, `${file}: OK` should be printed.

<details>
<summary>Checksum Information (click to expand)</summary>

> Checksums are not a security check, they validate file consistency
> (i.e. ensures the local file is the same as what is stored on the remote host).
>
> For file authenticity, see [GPG Signature](#GPG Signature)

Checksums are rather simple to use, and are great for your sanity.

There are many possible reasons for a file to have changed between hosts,
from Man In The Middle attacks, to connection instability, or even the sun simply doing its thing
(This topic is too complex for a README, but in short, space particles hit drives and sometimes corrupt data).

A checksum check will fail if either the file, or the checksum has changed.
If a checksum check fails, you should delete the local copy of both files and fetch them again.

</details>

#### GPG Signature

To verify a GPG signature, you need to use a keyring which already containers the public key.

##### Import GPG Public Key

<details>
<summary>Fingerprint (click to expand)</summary>

```
pub   rsa4096 2022-04-11 [SC] [expires: 2027-04-10]
      17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA
uid           [ unknown] jlisher packaging <packaging@jlisher.com>
```

</details>

<details>
<summary>GPG Public Key (click to expand)</summary>

```
-----BEGIN PGP PUBLIC KEY BLOCK-----

mQINBGJUsLEBEAC9OHslK/WSYORvNld/6P9lEjI/Wy8yHCrioHD864IgQHIQergT
egLjiJPc0QDzCpcAsJiWVmq/aIk1yf1THSzDJTDzucuqj9PznPQY56QTLcishLAm
HdPMszzN12RLsgREMPzh2lpgJFslVs4xDOSufWc/KeQGFIN5d4FIB6+VDVKOcsOZ
aRae6Z1tcsEaazc/sIQ4931EbD9IoftrXOFqN4Jv/DNH8qKFJ0DgwMKfJAAkeIgi
DiAxxYw5KlklbvzRFweufoLrIXF52emS4T5azZp7tOgqApqBCT65UNrX9x58Hy9v
n/rauU0Q460vIl6bEGO7aHlrPlXj98prjsx/ytl6q6iHeqCBsxMnjDyD9XILXYVr
K5NO3nytMWXUVDfefOfFsA7uxETKYqIuLLPteLp+vQZckFdQfbjSvD3yvpefbTnl
myG9/MlEH1STkqtM1QOSjVrPAm/g7lGIF3VGswGR/zdV/ugskvoBIXFFXSm95lFe
eg1UU4Uqi5cnLbR433J1DggB6ITVKbn7C5H0o5st6lxGv+jPdodWSK3cYKZNDKZo
uhZwcQRi9PfTUb/V5ueIGJ34lqAFYxgFGKpQ0F5HcryqzZt5YX2TWLKeRQIjAoyI
sOJJg8hnh0o4xm/Lu4a1GhnnnYxKCm4axRVK3O9DTcokB/L3dEd4+qU0owARAQAB
tClqbGlzaGVyIHBhY2thZ2luZyA8cGFja2FnaW5nQGpsaXNoZXIuY29tPokCWAQT
AQgAQhYhBBfFfLY8ryC+y5C5zEyTDVt566vKBQJiVLCxAhsDBQkJZgGABQsJCAcC
AyICAQYVCgkICwIEFgIDAQIeBwIXgAAKCRBMkw1beeurynUXEACz7kEiws+I4vsI
WU00bW+xb/a/gnJIyb2l/KUPtMOLLTynjnv48L7wIkntd+xOQ2n6Bu74VI6u5ulY
aBjDFMhrIZ5LlAW6jFiDluTaktkTXStmKhCexoAP7NVcI/YDcLSkQynYAiB6GFvW
FJlrINpyP8ePJAoxYaOAraobnbkZl8f8YUYHp6CtYBo+sE/I3jo+v41ahHiy69mx
sX4s2BotcPH50qOjIxBGGLOIkmQbqQiVw49aaFE0lw+UgmkYCuHWV40jU5gJB6CJ
ZiTq8h3oEQY1Pus8di1lmrAKVc2dKwZr3GKBAXlGC7GpSH8Jz+i2JlWFM0AeD/to
8TYljZhrp41T0E31r7zZJro6X5mW7dIk/kAUmVdLAKx3zIW+dlHmn7b0PkI8XBA9
yNr4LSRgWOouPePBliYBl/37Zxt/MHcc5fifA2PuFxf32Cnva9FKV9uhnamcI+iT
Lqzh5M6Vsa0MoW06ZEyzOM8m+vtFivCxvvAmWLWg5bX5kNJsDJVzEODaCMWtDTc/
vvwCHpl1F9yT/9JezeBjfGuUJS4lQ3+jh8E6psAZKi24pY8QrDVqgSxEtEQhDrr1
GJ6UvgZN5eGBh7u2tQMJKF3MfvKlks/QVTru/4urj+ZDQIxUV9vCC1RE7XVtlvaG
1PZgOFehQCB/Z6wG404L2ke5lpaQwg==
=/lRA
-----END PGP PUBLIC KEY BLOCK-----
```

The GPG public key is made available from the following locations:

- [git repo][git-repo-gpg-pub-key]: File
- [sources release host][src-host-gpg-pub-key]: File
- [rpm repo host][rpm-host-gpg-pub-key]: File

<details>
<summary>More Locations (click to expand)</summary>

The GPG public key is also available in the following packaged locations:

- [Source Releases][src-releases]: Archive
- RPM Packages:
  - [EL 8][rpm-package-gpg-el8]: Package
  - [EL 9][rpm-package-gpg-el9]: Package

</details>

There is also a [binary format public keyring][src-host-gpg-pub-keyring] available, if you would prefer an unarmoured format for some reason.

</details>

> Replace `gpg_key_filepath`, `gpg_fingerprint`, and `gpg_keyring_filename` where applicable.

Importing and trusting a public key manually is the recommended method to ensure trust.

Import a public key and verify the fingerprint into your default keyring:

```sh
gpg_key_filepath="${PWD}/jlisher-packaging.pub"
gpg_fingerprint="17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA"
gpg --import "${gpg_key_filepath}"
gpg --list-keys "${gpg_fingerprint}"
```

<details>
<summary>More Information (click to expand)</summary>

Verify the fingerprint using an existing alternative keyring:

```sh
gpg_keyring_filepath="${PWD}/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
gpg_fingerprint="17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA"
gpg --no-default-keyring --keyring "${gpg_keyring_filepath}" --list-keys "${gpg_fingerprint}"
```

Import a public key and verify the fingerprint into temp keyring:

```sh
gpg_keyring_filepath="$(mktemp)"
gpg_key_filepath="${PWD}/jlisher-packaging.pub"
gpg_fingerprint="17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA"
gpg --no-default-keyring --keyring "${gpg_keyring_filepath}" --import "${gpg_key_filepath}"
gpg --no-default-keyring --keyring "${gpg_keyring_filepath}" --list-keys "${gpg_fingerprint}"
```

</details>

##### Verify GPG Signature

> Replace `filename` and `gpg_keyring_filename` where applicable.

Simply use the `--verify` gpg command.

Using default keyring:

```sh
filename="install-jlisher-el-repo.sh"
gpg --verify "${filename}.asc" "${filename}"
```

<details>
<summary>More Information (click to expand)</summary>

Using `gpgv` with an existing keyring (also, for temp keyring):

```sh
gpg_keyring_filename="${PWD}/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
filename="install-jlisher-el-repo.sh"
gpgv --keyring "${gpg_keyring_filename}" "${filename}.asc" "${filename}"
```

Using an existing alternative keyring (also, for temp keyring):

```sh
gpg_keyring_filename="${PWD}/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg"
filename="install-jlisher-el-repo.sh"
gpg --no-default-keyring --keyring "${gpg_keyring_filename}" --verify "${filename}.asc" "${filename}"
```

> These may give a warning, if the key isn't trusted.

</details>

### Download & Install

#### Install Script (easiest, if you trust me)

```shell
pushd "$(mktemp -d)"
curl --remote-name-all --location "https://rpms.jlisher.com/el/install-jlisher-el-repo.sh{,.sha256sum}"
sha256sum -c "${PWD}/install-jlisher-el-repo.sh.sha256sum"
bash "${PWD}/install-jlisher-el-repo.sh"
popd
rm -rf "${OLDPWD}"
```

#### RPM Package Install (recommended)

Simply download the package's `.rpm` file, GPG signature, checksum, and checksum GPG signature:

Please read through the [install script][git-repo-install-script] for the steps to follow.

#### Direct DNF Install

You can also just install the `jlisher-gpg-keys` and `jlisher-fedora-repo` packages directly using:

```sh
dnf install \
  "https://rpms.jlisher.com/el/$(rpm -E %rhel)/jlisher-gpg-keys-latest.el(rpm -E %rhel).noarch.rpm" \
  "https://rpms.jlisher.com/el/$(rpm -E %rhel)/jlisher-el-repo-latest.el(rpm -E %rhel).noarch.rpm"
```

This is useful for testing in a container or a VM.

#### Direct Repo Install

There is also a `jlisher-el.repo` file available on the [RPM Host][rpm-host] which can simply be installed to the `/etc/yum.repos.d` directory.

```sh
curl --output "/etc/yum.repos.d/jlisher-el.repo" --location "https://rpms.jlisher.com/jlisher-el.repo"
```

### Enabling Repo

By default, the repo is not enabled. You can enable the repo using either:

```sh
dnf config-manager --set-enabled jlisher-el
```

or, for systems without dnf config-manager:

```sh
sed -i -E -e '3,6s/^(enabled)\=.*$/\1\=1/' /etc/yum.repos.d/jlisher-el.repo
```

You can also enable the repo during an installation:

```sh
dnf install --enablerepo jlisher-el PACKAGE
```

##

[git-repo-gpg-pub-key]: https://gitlab.com/jlisher/jlisher-el-repo/-/raw/master/rpm-gpg/RPM-GPG-KEY-jlisher-packaging
[git-repo-install-script]: https://gitlab.com/jlisher/jlisher-el-repo/-/raw/master/install-jlisher-el-repo.sh
[src-host-gpg-pub-key]: https://sources.jlisher.com/jlisher-packaging.pub
[src-host-gpg-pub-keyring]: https://sources.jlisher.com/gpgkey-17C57CB63CAF20BECB90B9CC4C930D5B79EBABCA.gpg
[src-releases]: https://sources.jlisher.com/jlisher-el-repo/
[rpm-host]: https://rpms.jlisher.com/
[rpm-host-gpg-pub-key]: https://rpms.jlisher.com/jlisher-packaging.pub
[rpm-package-gpg-el8]: https://rpms.jlisher.com/el/8/jlisher-gpg-keys-latest.el8.noarch.rpm
[rpm-package-gpg-el9]: https://rpms.jlisher.com/el/9/jlisher-gpg-keys-latest.el9.noarch.rpm
[coreutils]: https://src.fedoraproject.org/rpms/coreutils
[fedora-packaging]: https://docs.fedoraproject.org/en-US/packaging-guidelines/
[curl]: https://src.fedoraproject.org/rpms/curl
[wget]: https://src.fedoraproject.org/rpms/wget
[gnupg2]: https://src.fedoraproject.org/rpms/gnupg2
[dnf]: https://src.fedoraproject.org/rpms/dnf
